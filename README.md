# Triangles
Returns the area of a triangle given 6 points as arguments. Cloned from https://git.uwaterloo.ca/se1011/triangles

Changes:
- added condition to exit program with message if less than 6 arguments are given